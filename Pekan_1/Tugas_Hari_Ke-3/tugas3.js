//------soal 1-------------//

var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

 function UpperCaseFirstLetter(str){
     return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}
kataKedua = UpperCaseFirstLetter(kataKedua);
var gabungKata = kataPertama+' '+kataKedua+' '+kataKetiga+' '+kataKeempat.toUpperCase();
console.log(gabungKata);



//------------soal 2 --------------//
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

kataPertama = Number(kataPertama);
kataKedua = Number(kataKedua);
kataKetiga = Number(kataKetiga);
kataKeempat = Number(kataKeempat);
var jumlah = kataPertama+kataKedua+kataKetiga+kataKeempat;
console.log(jumlah);


//------------Soal 3----------------//
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); // do your own! 
var kataKetiga = kalimat.substring(15, 19); // do your own! 
var kataKeempat = kalimat.substring(19, 25); // do your own! 
var kataKelima = kalimat.substring(25, 32); // do your own! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);


//----------------soal 4 ---------------//
var nilai = 52;
if(nilai >= 80){
	console.log("indeksnya A")
}else if (nilai >= 70 && nilai < 80){
	console.log("indeksnya B")
}else if (nilai >= 60 && nilai < 70){
	console.log("indeksnya C")
}else if (nilai >= 50 && nilai < 60){
	console.log("indeksnya D")
}else{
	console.log("indeksnya E")
}


///-----------------soal 5 ----------------//
var hari = 22; 
var bulan = 7; 
var tahun = 2020;

if(hari > 0 && hari < 32){
	if(bulan > 0 && bulan < 13){
		if(tahun >= 1900 && bulan <= 2200){
			switch(bulan){
				case 1 : { console.log(hari + ' Januari '+tahun);break;}
				case 2 : { console.log(hari + ' Februari '+tahun);break;}
				case 3 : { console.log(hari + ' Maret '+tahun);break;}
				case 4 : { console.log(hari + ' April '+tahun);break;}
				case 5 : { console.log(hari + ' Mei '+tahun);break;}
				case 6 : { console.log(hari + ' Juni '+tahun);break;}
				case 7 : { console.log(hari + ' Juli '+tahun);break;}
				case 8 : { console.log(hari + ' Agustus '+tahun);break;}
				case 9 : { console.log(hari + ' September '+tahun);break;}
				case 10 : { console.log(hari + ' Oktober '+tahun);break;}
				case 11 : { console.log(hari + ' November '+tahun);break;}
				case 12 : { console.log(hari + ' Desember '+tahun);break;}
			}
				
		}else{
			console.log('tahun tidak sesuai');
		}
	}else{
		console.log('bulan tidak sesuai');
	}
	
}else{
	console.log('tanggal tidak sesuai');
}