//---soal no 1 --------//
var jumlah = 1;
console.log(' LOOPING PERTAMA '); 
while(jumlah <= 20) { 
	if(jumlah%2==0){
		console.log(jumlah + ' I love coding '); 
	}
	jumlah++;
}

var jumlah = 20;
console.log(' LOOPING kedua '); 
while(jumlah > 0) { 
	if(jumlah%2==0){
		console.log(jumlah + ' I will become a mobile developer '); 
	}
	jumlah--;
}


//-------------soal no 2--------------//
console.log('output' );
for(var angka = 1; angka <= 20; angka++) {
	if(angka==1) {
		console.log(angka + ' - Santai');
	}else{
		if(angka%2==1){
			if(angka%3==0){
				console.log(angka + ' - I Love Coding');
			}else{
				console.log(angka + ' - Santai');
			}
		}else if(angka%2==0){
			console.log(angka + ' - Berkualitas');
		}
	}
} 


//---------------soal no 3 ----------------//
var jumlah = 1;
console.log(' OUTPUT LOOPING TANGGA '); 

while(jumlah <= 7) { 
	var awal = 1;
	var bintang = '#';
	var samping = 1;
	while(samping < jumlah) { 
		
		samping ++;
		bintang =bintang+'#';
	}
	console.log(bintang); 
	jumlah++;
}


//------------soal no 4 -------------------//
var kalimat="saya sangat senang belajar javascript";
var kalimat = kalimat.split(" ");
console.log(kalimat) 
//----------------soal no 5 ----------------//
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort()
console.log(daftarBuah) 