function luasLingkaran(r){
	var luas = 3.14*r*r
	
	return luas;
}

function luasSegitiga(a, t){
	var luas = 0.5 * a * t
	
	return luas;
}

function luasPersegi(s){
	var luas = s * s
	
	return luas;
}
console.log('luas lingkaran');
console.log(luasLingkaran(7));

console.log('luas segitiga');
console.log(luasSegitiga(6, 7));

console.log('luas persegi');
console.log(luasPersegi(6));