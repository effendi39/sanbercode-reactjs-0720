//------------Soal No 1 -----------------//
function teriak() {
  return "Halo Sanbers!";
}
console.log(teriak())


//----------------Soal No 2 --------------//
function kalikan(num1, num2) {
	 if((typeof num1 == 'number') && (typeof num2 == 'number')){
		return num1*num2;
	}else{
		return "Inputan tidak berisi angka";
	}
  
}

var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48

//------------------SOal No 3 ---------------------//
function introduce(name, age, address, hobby) {
	return '"Nama saya '+ name+', umur saya '+age+', alamat saya di '+address+', dan saya punya hobby yaitu '+hobby+'!" ';
  
}

var name = "Agus"
var age = 30
var address = "Jln Belum jadi"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 

