/*   Soal No 1 */
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]
var arrayDaftarPeserta = {
    nama : "John",
    jenisKelamin : "Doe",
    hobi: "male",
    tahunLahir: 27
} 

/*   Soal No 2 */
var dataBuah = [
{	nama: "strawberry",
	warna: "merah",
	adaBijinya: "tidak",
	harga: 9000 },
{	nama: "jeruk",
	warna: "oranye",
	adaBijinya: "ada",
	harga: 8000},
{	nama: "Semangka",
	warna: "Hijau & Merah",
	adaBijinya: "ada",
	harga: 10000},
{	nama: "Pisang",
	warna: "Kuning",
	adaBijinya: "tidak",
	harga: 5000}];
	
	console.log(dataBuah[0]);
	
/*   Soal No 3 */
var dataFilm = [{nama: "Ayat ayat cinta", durasi: "2 jam", genre: "Romance", tahun: "2011"}]
dataFilm.forEach(function(item){
   console.log("nama : " + item.nama)
})


/*    SOal no 4 */
class Animal {
	constructor(nama) {
		this._name = nama;
		this.leg = 4;
		this.cold_blooded = false;
	}
	get name() {
		return this._nama;
	}
	set name(name) {
		this._nama = nama;
	}
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false


// Code class Ape dan class Frog di sini
 class Ape extends Animal{
	constructor(name){
		super(name);
		this.leg = 2;
	}
	yell(){
		return "Auooo";
	}
 }
 
 class frog extends Animal{
	constructor(name){
		super(name);
		this.leg = 2;
	}
	jump(){
		return "hop hop";
	}
 }
 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 


/*  Soal No 5   */
function Clock({ template }) {
  
  var timer;

  function render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  this.stop = function() {
    clearInterval(timer);
  };

  this.start = function() {
    render();
    timer = setInterval(render, 1000);
  };

}

var clock = new Clock({template: 'h:m:s'});
clock.start();


class Clock {
    constructor({template}){
		clock(){
			var timer;
			function render(){
				var date = new Date();

				var hours = date.getHours();
				if (hours < 10) hours = '0' + hours;

				var mins = date.getMinutes();
				if (mins < 10) mins = '0' + mins;

				var secs = date.getSeconds();
				if (secs < 10) secs = '0' + secs;

				var output = template
				  .replace('h', hours)
				  .replace('m', mins)
				  .replace('s', secs);

				console.log(output);
			}
			this.stop = function(){
				clearInterval(timer);
			}
			this.start = function(){
				render();
				timer = setInterval(render, 10000);
			}
		}
	}
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 